document.addEventListener('DOMContentLoaded', () => {
  var data = [
    {x: 1979, y: 27},
    {x: 1980, y: 18},
    {x: 1981, y: 23},
    {x: 1982, y: 21},
    {x: 1983, y: 27},
    {x: 1984, y: 29},
    {x: 1985, y: 30},
    {x: 1986, y: 28},
    {x: 1987, y: 52},
    {x: 1988, y: 90},
    {x: 1989, y: 40},
    {x: 1990, y: 59},
    {x: 1991, y: 65},
    {x: 1992, y: 60},
    {x: 1993, y: 80},
    {x: 1994, y: 83},
    {x: 1995, y: 115},
    {x: 1996, y: 110},
    {x: 1997, y: 117},
    {x: 1998, y: 127},
    {x: 1999, y: 127},
    {x: 2001, y: 171},
    {x: 2002, y: 216},
    {x: 2003, y: 278},
    {x: 2004, y: 294},
    {x: 2005, y: 337},
    {x: 2006, y: 402},
    {x: 2007, y: 392},
    {x: 2008, y: 413},
    {x: 2009, y: 436},
    {x: 2010, y: 430},
    {x: 2011, y: 478},
    {x: 2012, y: 625},
    {x: 2013, y: 538},
    {x: 2014, y: 577},
    {x: 2015, y: 549},
    {x: 2016, y: 415},
  ];

  var chart = new Chart('chart', {
    type: 'line',
    data: {
      datasets: [{
        label: 'Citations per year',
        data: data
      }]
    },
    options: {
      defaultFontColor: '#000',
      scales: {
        xAxes: [{
          type: 'linear',
          position: 'bottom'
        }]
      }
    }
  });
}, false);
